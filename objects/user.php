<?php
class User{
 
    // database connection and table name
    private $conn;
    private $table_name = "users";
 
    // object properties
    public $email;
    public $auth_token;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read users
    function read(){
    
        // select all query
        $query = "SELECT id_user, email, auth_token FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create new user record
    function create(){
    
        // insert query
        $query = "INSERT INTO " . $this->table_name . " SET email = :email, auth_token = :auth_token";
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->auth_token=htmlspecialchars(strip_tags($this->auth_token));
    
        // bind the values
        $stmt->bindParam(':email', $this->email);
    
        // hash the password before saving to database
        $auth_token_hash = password_hash($this->auth_token, PASSWORD_BCRYPT);
        $stmt->bindParam(':auth_token', $auth_token_hash);
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }
 
    // check if given email exist in the database
    function emailExist(){
    
        // query to check if email exists
        $query = "SELECT auth_token FROM " . $this->table_name . " WHERE email = ? LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->auth_token = $row['auth_token'];
    
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }

    function updateEmail($email){
 
        // update query
        $query = "UPDATE " . $this->table_name . " 
                    SET email = :newEmail WHERE email = :email";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // bind new values
        $stmt->bindParam(':newEmail', $email);
        $stmt->bindParam(':email', $this->email);

        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

}