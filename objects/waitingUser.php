<?php
class WaitingUser{
 
    // database connection and table name
    private $conn;
    private $table_name = "waitingusers";
 
    // object properties
    public $id_waiting_user;
    public $email;
    public $validation_code;
    public $dead_line; 
    public $role;
    public $id_money_box;

 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
/*
    // read users
    function read(){
    
        // select all query
        $query = "SELECT id_user, email, auth_token FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }*/

    // create new waiting user record
    function create(){
        
        // insert query
        $query = "INSERT INTO " . $this->table_name . " SET email = :email, validation_code = :validation_code,
                                                        dead_line = :dead_line, role = :role, id_money_box = :id_money_box ";
        // prepare the query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->role=htmlspecialchars(strip_tags($this->role));


        // bind the values
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':validation_code', $this->validation_code);
        $stmt->bindParam(':dead_line', $this->dead_line);
        $stmt->bindParam(':role', $this->role);
        $stmt->bindParam(':id_money_box', $this->id_money_box);
      
        // execute the query, also check if query was successful
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    // check if given email exist in the database
    function emailExist(){
    
        // query to check if email exists
        $query = "SELECT id_waiting_user, validation_code, dead_line, role, id_money_box FROM " . $this->table_name . " WHERE email = ? LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->id_waiting_user = $row['id_waiting_user'];
            $this->validation_code = $row['validation_code'];
            $this->dead_line = $row['dead_line'];
            $this->role = $row['role'];
            $this->id_money_box = $row['id_money_box'];
    
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }

    // check if given email exist in the database
    function isConfirmed(){
    
        // query to check if email exists
        $query = "SELECT id_waiting_user, validation_code, dead_line, role, id_money_box FROM " . $this->table_name . " WHERE email = ?";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();

        $confimed = false; 
    
        while(!$confimed && $num>0 )
        {
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if(($this->validation_code == $row['validation_code']) && (time() < strtotime($row['dead_line'])))
            {
                // assign values to object properties
                $this->id_waiting_user = $row['id_waiting_user'];
                $this->dead_line = $row['dead_line'];
                $this->role = $row['role'];
                $this->id_money_box = $row['id_money_box'];
                $confimed = true;
            }
            $num = $num - 1;
        }
        return  $confimed;
    }

    // delete the waitingUser
    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id_waiting_user = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id_waiting_user);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}