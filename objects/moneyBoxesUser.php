<?php
class MoneyBoxesUser{
 
    // database connection
    private $conn;
    private $table_name =  "moneyboxesusers";
 
    // object properties

    public $email;
    public $id_money_box;
    public $role;

    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read users
    /*
    function read(){
    
        // select all query
        $query = "SELECT id_user, email, auth_token FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }*/

    // create new user record
    function create(){
    
        $correctExecution = false;
        // insert query
        $query = "INSERT INTO " . $this->table_name . " SET `email` = :email , id_money_box = :id_money_box ,`role` = :role ";
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->role=htmlspecialchars(strip_tags($this->role));
    
        // bind the values
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':id_money_box', $this->id_money_box);
        $stmt->bindParam(':role', $this->role);

        // execute the query, also check if query was successful
        if($stmt->execute()){
            $correctExecution = true;
        }
    
        return $correctExecution;
    }

    
    // check if given email exist in the database
    function emailIdExist(){
    
        // query to check if email exists
        $query = "SELECT role FROM " . $this->table_name . " WHERE email = ? AND id_money_box = ? LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
        $stmt->bindParam(2, $this->id_money_box);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->role = $row['role'];
    
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }

    
    function readMoneybox(){
    
        // select all query
        $query = "SELECT id_money_box FROM " . $this->table_name . " WHERE email = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // bind id of record to delete
        $stmt->bindParam(1, $this->email);
    
        // execute query
        if($stmt->execute())
    
        return $stmt;
    }

    function deleteAllUser(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id_money_box = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id_money_box);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    function updateEmail($email){
 
        // update query
        $query = "UPDATE " . $this->table_name . " 
                    SET email = :newEmail WHERE email = :email";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // bind new values
        $stmt->bindParam(':newEmail', $email);
        $stmt->bindParam(':email', $this->email);

        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

}