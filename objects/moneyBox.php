<?php
class MoneyBox{
 
    // database connection and table name
    private $conn;
    private $table_name = "moneyboxes";
 
    // object properties
    public $id_money_box = -1;
    public $ssid;
    public $coin001;
    public $coin002;
    public $coin005;
    public $coin010;
    public $coin020;
    public $coin050;
    public $coin100;
    public $coin200;
    public $banknote5;
    public $banknote10;
    public $banknote20;
    public $banknote50;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read users
    /*
    function read(){
    
        // select all query
        $query = "SELECT id_user, email, auth_token FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }*/

    // create new user record
    function create(){
    
        $correctExecution = false;
        // insert query
        $query = "INSERT INTO " . $this->table_name . " SET ssid = :ssid ,
            `0,01coin` = :coin001 ,`0,02coin` = :coin002 ,`0,05coin` = :coin005 ,
            `0,10coin` = :coin010 ,`0,20coin` = :coin020 ,`0,50coin` = :coin050 ,
            `1,00coin` = :coin100 ,`2,00coin` = :coin200 , 5banknote = :banknote5 , 
            10banknote = :banknote10 , 20banknote = :banknote20 , 50banknote = :banknote50 ";
        // prepare the query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->ssid=htmlspecialchars(strip_tags($this->ssid));
    
        // bind the values
        $stmt->bindParam(':ssid', $this->ssid);
        $stmt->bindParam(':coin001', $this->coin001);
        $stmt->bindParam(':coin002', $this->coin002);
        $stmt->bindParam(':coin005', $this->coin005);
        $stmt->bindParam(':coin010', $this->coin010);
        $stmt->bindParam(':coin020', $this->coin020);
        $stmt->bindParam(':coin050', $this->coin050);
        $stmt->bindParam(':coin100', $this->coin100);
        $stmt->bindParam(':coin200', $this->coin200);
        $stmt->bindParam(':banknote5', $this->banknote5);
        $stmt->bindParam(':banknote10', $this->banknote10);
        $stmt->bindParam(':banknote20', $this->banknote20);
        $stmt->bindParam(':banknote50', $this->banknote50);

        // execute the query, also check if query was successful
        if($stmt->execute()){
            $this->id_money_box = $this->conn->lastInsertId();
            $correctExecution = true;
        }
    
        return $correctExecution;
    }
 
    
    // check if given id exist in the database
    function idExist(){
    
        // query to check if id exist
        $query = "SELECT id_money_box, ssid, `0,01coin`, `0,02coin`, `0,05coin`, `0,10coin`, `0,20coin`, `0,50coin`, `1,00coin`, `2,00coin`, 5banknote, 10banknote, 20banknote, 50banknote  FROM " . $this->table_name . " WHERE id_money_box = ? LIMIT 0,1";
    
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // bind given id value
        $stmt->bindParam(1, $this->id_money_box);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if id exist, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
            // assign values to object properties
            $this->ssid = $row['ssid'];
            $this->coin001 = $row['0,01coin'];
            $this->coin002 = $row['0,02coin'];
            $this->coin005 = $row['0,05coin'];
            $this->coin010 = $row['0,10coin'];
            $this->coin020 = $row['0,20coin'];
            $this->coin050 = $row['0,50coin'];
            $this->coin100 = $row['1,00coin'];
            $this->coin200 = $row['2,00coin'];
            $this->banknote5 = $row['5banknote'];
            $this->banknote10 = $row['10banknote'];
            $this->banknote20 = $row['20banknote'];
            $this->banknote50 = $row['50banknote'];
    
            // return true because id exist in the database
            return true;
        }
    
        // return false if id does not exist in the database
        return false;
    }

    function update(){
 
        // update query
        $query = "UPDATE " . $this->table_name . " 
                    SET `0,01coin` = :coin001 ,`0,02coin` = :coin002 ,`0,05coin` = :coin005 ,
                    `0,10coin` = :coin010 ,`0,20coin` = :coin020 ,`0,50coin` = :coin050 ,
                    `1,00coin` = :coin100 ,`2,00coin` = :coin200 , 5banknote = :banknote5 , 
                    10banknote = :banknote10 , 20banknote = :banknote20 , 50banknote = :banknote50  WHERE id_money_box = :id_money_box";
     
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // bind new values
        $stmt->bindParam(':coin001', $this->coin001);
        $stmt->bindParam(':coin002', $this->coin002);
        $stmt->bindParam(':coin005', $this->coin005);
        $stmt->bindParam(':coin010', $this->coin010);
        $stmt->bindParam(':coin020', $this->coin020);
        $stmt->bindParam(':coin050', $this->coin050);
        $stmt->bindParam(':coin100', $this->coin100);
        $stmt->bindParam(':coin200', $this->coin200);
        $stmt->bindParam(':banknote5', $this->banknote5);
        $stmt->bindParam(':banknote10', $this->banknote10);
        $stmt->bindParam(':banknote20', $this->banknote20);
        $stmt->bindParam(':banknote50', $this->banknote50);
        $stmt->bindParam(':id_money_box', $this->id_money_box);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
    }

    function delete(){
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id_money_box = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id_money_box);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}