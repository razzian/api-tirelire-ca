<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/waitingUser.php';
include_once '../../objects/moneyBoxesUser.php';
include_once '../../objects/User.php';

 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// instantiate waitingUser object
$waitingUser = new WaitingUser($db);
// set waitingUser property values
$waitingUser->email =  $data->email;
$waitingUser->validation_code = $data->validation_code;
if($waitingUser->isConfirmed())
{
    // instantiate User object
    $user = new User($db);
    
    // set User property values
    $user->email = $waitingUser->email;
    $user->auth_token = $data->auth_token;
    
    // create the user
    if($user->emailExist() || $user->create()){
         // instantiate moneyBoxIdUser object
        $moneyBoxesUser = new MoneyBoxesUser($db);

        // set moneyBoxIdUser property values
        $moneyBoxesUser->email = $waitingUser->email;
        $moneyBoxesUser->id_money_box = $waitingUser->id_money_box;
        $moneyBoxesUser->role = $waitingUser->role;
            // create the moneyBoxIdUser
        if($moneyBoxesUser->create()){

            // delete the waitingUser
            $waitingUser->delete();

            // set response code
            http_response_code(200);

            // display message: MoneyBox was created
            echo json_encode(array("message" => "user has been confimed",
                                    "id_money_box" => $waitingUser->id_money_box));

        }
        // message if unable to create moneyBoxIdUser
        else{

            // set response code
            http_response_code(400);

            // display message
            echo json_encode(array("message" => "Unable to create user in relation table"));
        }
    }
    // message if unable to create user
    else{
    
        // set response code
        http_response_code(400);
    
        // display message
        echo json_encode(array("message" => "Unable to create user"));
    }
}
else{
    // set response code
    http_response_code(403);
 
    // tell the user Confirmation failed
    echo json_encode(array("message" => "Confirmation failed"));
}
?>