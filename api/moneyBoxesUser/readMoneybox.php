<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt-master/src/BeforeValidException.php';
include_once '../../libs/php-jwt-master/src/ExpiredException.php';
include_once '../../libs/php-jwt-master/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/moneyBoxesUser.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt){

    try {
 
        // try decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        // jwt ok

        // instantiate MoneyBoxesUser object 
        $moneyBoxesUser = new MoneyBoxesUser($db);

        // set MoneyBoxesUser property values
        $moneyBoxesUser->email = $decoded->data->email;

        // query moneyboxes
        $stmt = $moneyBoxesUser->readMoneybox();
        $num = $stmt->rowCount();
        
        // check if more than 0 record found
        if($num>0){
        
            // moneyboxes array
            $moneybox_arr=array();
            $moneybox_arr["records"]=array();
        
            // retrieve our table contents
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                // extract row
                extract($row);
        
                $moneybox_item=array(
                    "id_money_box" => $id_money_box,
                );
        
                array_push($moneybox_arr["records"], $moneybox_item);
            }
        
            // set response code - 200 OK
            http_response_code(200);
        
            // show moneyboxes data in json format
            echo json_encode($moneybox_arr);
        }
        else{

            // set response code - 404 Not found
            http_response_code(404);
        
            // tell the user no moneyboxes found
            echo json_encode(
                array("message" => "No moneyboxes found")
            );
        }
    }
    // if decode fails, it means jwt is invalid
    catch (Exception $e){
    
        // set response code
        http_response_code(401);
    
        // show error message
        echo json_encode(array(
            "message" => "Access denied",
            "error" => $e->getMessage()
        ));
    }
}
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>