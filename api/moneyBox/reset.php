<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/moneyBox.php';
include_once '../../objects/moneyBoxesUser.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// instantiate moneyBox object
$moneyBox = new MoneyBox($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// set moneyBox property values
$moneyBox->id_money_box = $data->id_money_box;
if($moneyBox->idExist() && ($data->ssid == $moneyBox->ssid)){
    if($moneyBox->delete())
    {
        $moneyBoxesUser = new MoneyBoxesUser($db);
        $moneyBoxesUser->id_money_box = $data->id_money_box;
        if($moneyBoxesUser->deleteAllUser())
        {
            $moneyBox->coin001 = 0;
            $moneyBox->coin002 = 0;
            $moneyBox->coin005 = 0;
            $moneyBox->coin010 = 0;
            $moneyBox->coin020 = 0;
            $moneyBox->coin050 = 0;
            $moneyBox->coin100 = 0;
            $moneyBox->coin200 = 0;
            $moneyBox->banknote5 = 0;
            $moneyBox->banknote10 = 0;
            $moneyBox->banknote20 = 0;
            $moneyBox->banknote50 = 0;
            // create the moneyBox
            if($moneyBox->create()){
        
                // set response code
                http_response_code(200);
                    
                // display message: MoneyBox was created
                echo json_encode(array("message" => "MoneyBox was reset", "id" => $moneyBox->id_money_box));
            }
            // message if unable to create moneyBox
            else{
            
                // set response code
                http_response_code(400);
            
                // display message: unable to create moneyBox
                echo json_encode(array("message" => "Unable to create moneyBox."));
            }
        } 
        else{
            // set response code
            http_response_code(400);
        
            // display message: unable to create moneyBox
            echo json_encode(array("message" => "Unable to delet previous users of the moneyBox."));
        }
        
    }
    else{
        // set response code
        http_response_code(400);
                
        // display message: unable to create moneyBox
        echo json_encode(array("message" => "Unable to delete previous moneyBox."));
    }
}
else
{   
    // set response code
    http_response_code(401);
    
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>