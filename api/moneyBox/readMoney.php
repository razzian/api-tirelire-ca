<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/moneyBoxesUser.php';
include_once '../../objects/moneyBox.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// instantiate moneyBox for authentifiaction
$moneyBox = new MoneyBox($db);
$moneyBox->id_money_box = $data->id_money_box;
if($moneyBox->idExist() && ($data->ssid == $moneyBox->ssid)){
    // set response code - 200 OK
    http_response_code(200);

    // show money in moneybox in json format
    echo json_encode( array(    "coin001" => $moneyBox->coin001,
                                "coin002" => $moneyBox->coin002,
                                "coin005" => $moneyBox->coin005,
                                "coin010" => $moneyBox->coin010,
                                "coin020" => $moneyBox->coin020,
                                "coin050" => $moneyBox->coin050,
                                "coin100" => $moneyBox->coin100,
                                "coin200" => $moneyBox->coin200,
                                "banknote5" => $moneyBox->banknote5,
                                "banknote10" => $moneyBox->banknote10,
                                "banknote20" => $moneyBox->banknote20,
                                "banknote50" => $moneyBox->banknote50));
}
else
{
    
    // set response code
    http_response_code(401);
    
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>?>