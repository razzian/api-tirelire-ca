<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/moneyBoxesUser.php';
include_once '../../objects/moneyBox.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// instantiate moneyBox for authentifiaction
$moneyBox = new MoneyBox($db);
$moneyBox->id_money_box = $data->id_money_box;
if($moneyBox->idExist() && ($data->ssid == $moneyBox->ssid)){

    $moneyBox->coin001 = $data->coin001;
    $moneyBox->coin002 = $data->coin002;
    $moneyBox->coin005 = $data->coin005;
    $moneyBox->coin010 = $data->coin010;
    $moneyBox->coin020 = $data->coin020;
    $moneyBox->coin050 = $data->coin050;
    $moneyBox->coin100 = $data->coin100;
    $moneyBox->coin200 = $data->coin200;
    $moneyBox->banknote5 = $data->banknote5;
    $moneyBox->banknote10 = $data->banknote10;
    $moneyBox->banknote20 = $data->banknote20;
    $moneyBox->banknote50 = $data->banknote50;
    if($moneyBox->update())
    {
        // set response code - 200 OK
        http_response_code(200);

        // show money in moneybox in json format
        echo json_encode( array("message" => "values for coin and banknote are update"));
    }
    else
    {
        // set response code
        http_response_code(400);
            
        // tell the user access denied
        echo json_encode(array("message" => "Unable to update values for coin and banknote"));
    }

}
else
{   
    // set response code
    http_response_code(401);
    
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>?>