<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt-master/src/BeforeValidException.php';
include_once '../../libs/php-jwt-master/src/ExpiredException.php';
include_once '../../libs/php-jwt-master/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/moneyBoxesUser.php';
include_once '../../objects/moneyBox.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt){

    try {
 
        // try decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        // jwt ok

        // instantiate MoneyBoxesUser object
        $moneyBoxesUser = new MoneyBoxesUser($db);

        // set MoneyBoxesUser property values
        $moneyBoxesUser->email = $decoded->data->email;
        $moneyBoxesUser->id_money_box = $data->id_money_box;
  
        // find the moneyBoxesUser in data
        if( $moneyBoxesUser->emailIdExist()){
            $moneyBox = new MoneyBox($db);
            $moneyBox->id_money_box = $data->id_money_box;
            if( $moneyBox->idExist())
            {
                // set response code - 200 OK
                http_response_code(200);

                // show role of user in json format
                echo json_encode( array(    "coin001" => $moneyBox->coin001,
                                            "coin002" => $moneyBox->coin002,
                                            "coin005" => $moneyBox->coin005,
                                            "coin010" => $moneyBox->coin010,
                                            "coin020" => $moneyBox->coin020,
                                            "coin050" => $moneyBox->coin050,
                                            "coin100" => $moneyBox->coin100,
                                            "coin200" => $moneyBox->coin200,
                                            "banknote5" => $moneyBox->banknote5,
                                            "banknote10" => $moneyBox->banknote10,
                                            "banknote20" => $moneyBox->banknote20,
                                            "banknote50" => $moneyBox->banknote50));
            }
            else
            {
                 // set response code - 404 Not found
                http_response_code(404);
            
                // tell error
                echo json_encode(
                    array("message" => "this moneybox doesn't exist")
                );
            }

        }
        else{

            // set response code - 404 Not found
            http_response_code(404);
        
            // tell no user for this moneybox
            echo json_encode(
                array("message" => "the user is not found for this moneybox")
            );
        }
    }
    // if decode fails, it means jwt is invalid
    catch (Exception $e){
    
        // set response code
        http_response_code(401);
    
        // show error message
        echo json_encode(array(
            "message" => "Access denied",
            "error" => $e->getMessage()
        ));
    }
}
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>