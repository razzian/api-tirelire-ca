<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// required to encode json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt-master/src/BeforeValidException.php';
include_once '../../libs/php-jwt-master/src/ExpiredException.php';
include_once '../../libs/php-jwt-master/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/waitingUser.php';
include_once '../../objects/moneyBoxesUser.php';


// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// get jwt
$jwt=isset($data->jwt) ? $data->jwt : "";

// if jwt is not empty
if($jwt){

    try {
 
        // try decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        // jwt ok

        // instantiate MoneyBoxIdUser object to confirm role of the author of the request
        $moneyBoxesUser = new MoneyBoxesUser($db);

        // set MoneyBoxIdUser property values
        $moneyBoxesUser->email = $decoded->data->email;
        $moneyBoxesUser->id_money_box = $data->id_money_box;
        if( $moneyBoxesUser->emailIdExist() && (($moneyBoxesUser->role == "owner") || ($moneyBoxesUser->role == "parent")))
        {
            // instantiate waitingUser object
            $waitingUser = new WaitingUser($db);
            // set waitingUser property values
            $waitingUser->email =  $data->email;
            $waitingUser->validation_code = rand(10000000,99999999);
            $deadLineTS = time() + (24 * 60 * 60);
            $waitingUser->dead_line = date('Y-m-d H:i:s', $deadLineTS );
            $waitingUser->role = $data->role;
            $waitingUser->id_money_box = $data->id_money_box;

            // create the waitingUser
            if($waitingUser->create())
            {
                // confirmation message
                $message = "Vous avez réçament été enregistré en tant que " . $data->role . " d'une tirelire du Credit Agricole\r\nPour confirmer cette enregistrement veuillez aller sur l'application mobil de la tirelire et entrait le code de confirmation suivant :\r\n  " . $waitingUser->validation_code ;
                $message = wordwrap($message, 70, "\r\n");
                /*
                // send mail
                mail($waitingUser->email, 'Validation tirelire CA', $message);*/

                // set response code
                http_response_code(200);

                // display message
                echo json_encode(array("message" => "waitingUser was created and need confirmation"));

            }
            // message if unable to create waitingUser
            else{
            
                // set response code
                http_response_code(400);
            
                // display message: unable to create waitingUser
                echo json_encode(array("message" => "Unable to create waitingUser"));
            }
        }
        else{
            http_response_code(401);
    
            // show error message if role is not ok
            echo json_encode(array(
                "message" => "your role did not allow to add a user to this moneybox"
            ));
        }
    }
    // if decode fails, it means jwt is invalid
    catch (Exception $e){
    
        // set response code
        http_response_code(401);
    
        // show error message
        echo json_encode(array(
            "message" => "Access denied",
            "error" => $e->getMessage()
        ));
    }
}
// show error message if jwt is empty
else{
 
    // set response code
    http_response_code(401);
 
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>