<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// files needed to connect to database
include_once '../../config/database.php';
include_once '../../objects/waitingUser.php';
include_once '../../objects/moneyBox.php';

// get database connection
$database = new Database();
$db = $database->getConnection();
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// instantiate moneyBox for authentifiaction
$moneyBox = new MoneyBox($db);
$moneyBox->id_money_box = $data->id_money_box;
if($moneyBox->idExist() && ($data->ssid == $moneyBox->ssid)){
    
    // instantiate waitingUser object
    $waitingUser = new WaitingUser($db);
    // set waitingUser property values
    $waitingUser->email =  $data->email;
    $waitingUser->validation_code = rand(10000000,99999999);
    $deadLineTS = time() + (2 * 60 * 60);
    $waitingUser->dead_line = date('Y-m-d H:i:s', $deadLineTS );
    $waitingUser->role = "owner";
    $waitingUser->id_money_box = $data->id_money_box;

    // create the waitingUser
    if($waitingUser->create())
    {
        // confirmation message
        $message = "Vous avez réçament été enregistré en tant que propriétaire d'une tirlire du Credit Agricole\r\nPour confirmer cette enregistrement veuillez aller sur l'application mobil de la tirelire et entrait le code de confirmation suivant :\r\n  " . $waitingUser->validation_code ;
        $message = wordwrap($message, 70, "\r\n");
        /*
        // send email
        mail($waitingUser->email, 'Validation tirelire CA', $message);*/

        // set response code
        http_response_code(200);

        // display message
        echo json_encode(array("message" => "waitingUser was created and need confirmation"));

    }
    // message if unable to create waitingUser
    else{
    
        // set response code
        http_response_code(400);
    
        // display error message
        echo json_encode(array("message" => "Unable to create waitingUser"));
    }
}
else
{
 
    // set response code
    http_response_code(401);
 
    // tell the user access denied
    echo json_encode(array("message" => "Access denied"));
}
?>